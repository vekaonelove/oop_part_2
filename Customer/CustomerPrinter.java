package Customer;

import Customer.Customer;

public class CustomerPrinter {
    public void print(Customer[] customers){
        for(Customer customer: customers){
            print(customer);
        }
    }

    public void print(Customer customer){
        if (customer != null){
            System.out.println(customer.toString());
        }
    }
}
