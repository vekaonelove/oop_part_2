package Customer;

import Customer.Customer;

public class CustomerList {

    public CustomerList() {
    }

    public Customer[] sortInAlphabeticalOrder(Customer[] customers) {
        int n = customers.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (customers[j].getName().compareTo(customers[j + 1].getName()) > 0 ||
                        (customers[j].getName().compareTo(customers[j + 1].getName()) == 0 && customers[j].getSurname().compareTo(customers[j + 1].getSurname()) > 0 ||
                        (customers[j].getName().compareTo(customers[j + 1].getName()) == 0 && customers[j].getSurname().compareTo(customers[j + 1].getSurname()) == 0 &&
                                customers[j].getPatronymic().compareTo(customers[j + 1].getPatronymic()) > 0))) {
                    Customer temp = customers[j];
                    customers[j] = customers[j + 1];
                    customers[j + 1] = temp;
                }
            }
        }
        return customers;
    }

    public Customer[] getCustomersWithMatchingCardNumber(Customer[] customers, int start, int finish){
        Customer[] matchingCustomers = new Customer[customers.length];
        int counter = 0;

        for(Customer customer: customers){
            if(customer.getCardNumber() <= finish && customer.getCardNumber() >= start){
                matchingCustomers[counter] = customer;
                counter++;
            }
        }
        return matchingCustomers;
    }
}
