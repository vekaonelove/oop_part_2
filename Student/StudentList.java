package Student;

import Student.Student;

public class StudentList {
    private Student[] students;

    public StudentList(Student[] students) {
        this.students = students;
    }

    public Student[] findExcellentStudents() {
        Student[] result = new  Student[students.length];
        int count = 0;
        for (int i = 0; i < students.length; i++){
            if(students[i].isExcellentStudent()){
                result[count++] = students[i];
            }
        }

        Student[] destination = new Student[count];
        System.arraycopy(result, 0, destination, 0, count);
        return destination;
    }

}
