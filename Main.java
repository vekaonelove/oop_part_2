import Airline.Airline;
import Airline.AirlineList;
import Airline.AirlinePrinter;

import Book.Book;
import Book.BookRepository;
import Book.BookPrinter;

import Customer.Customer;
import Customer.CustomerList;
import Customer.CustomerPrinter;

import Student.Student;
import Student.StudentList;
import Student.StudentPrinter;

import Train.Train;
import Train.TrainList;
import Train.TrainPrinter;

public class Main {
    public static void main(String[] args) {

        //Task 1
        int[] goodStudentGrades = new int[]{9, 10, 10, 9, 10};
        int[] badStudentGrades = new int[]{4, 2, 3, 6, 8};
        Student[] students = new Student[]{
                new Student("Ivan", "Ivanov", 1, goodStudentGrades),
                new Student("Lana", "Del Rey", 2, goodStudentGrades),
                new Student("Alla", "Pugacheva", 3, badStudentGrades),
                new Student("King", "Arthur", 1, goodStudentGrades),
                new Student("Scarlett", "Johansonn", 3, badStudentGrades)

        };
        StudentList studentList = new StudentList(students);
        StudentPrinter studentPrinter = new StudentPrinter();

        Student[] excellentStudents = studentList.findExcellentStudents();
        studentPrinter.print(excellentStudents);
        System.out.println('\n');


        // Task 2
        Train[] trains = new Train[]{
                new Train("Milan", 125, 12, 30),
                new Train("Rome", 312, 14, 30),
                new Train("Rome", 313, 15, 45),
                new Train("Las-Vegas", 123, 18, 0),
                new Train("Tokyo", 617, 6, 15),
        };
        TrainList trainList = new TrainList(trains);
        TrainPrinter trainPrinter = new TrainPrinter();

        Train[] sortedByNumberTrains = trainList.sortByTrainNumber(trains);
        trainPrinter.print(sortedByNumberTrains);
        System.out.println();

        Train[] sortedByDestinationTrains = trainList.sortByDestination(trains);
        trainPrinter.print(sortedByDestinationTrains);
        System.out.println();

        int trainNumber = 312;
        trainPrinter.printByNumber(trainNumber, trains);

        System.out.println('\n');

        // Task 3
        Customer[] customers = new Customer[]{
                new Customer(1, "John", "Watson", "Mikhailovich", "Baker str. 221-B", 123456789, 123456789),
                new Customer(2, "Sherlock", "Holmes", "Sergeevich", "Baker str. 221-B", 122456789, 128456789),
                new Customer(3, "Ryan", "Gosling", "Ivanovich", "Maker str. 221-B", 123456779, 123456786),
                new Customer(4, "Robert", "De Niro", "Vasylievich", "Taker str. 221-B", 123456559, 123450789),
                new Customer(5, "Tyler", "Derden", "Vitalyevich", "Caker str. 221-B", 123456229, 123456689)
        };

        CustomerList customerList = new CustomerList();
        CustomerPrinter customerPrinter = new CustomerPrinter();

        Customer[] sortedCustomers = customerList.sortInAlphabeticalOrder(customers);
        customerPrinter.print(sortedCustomers);
        System.out.println();

        int startInterval = 0;
        int endInterval = 123456779;

        Customer[] matchingCustomers = customerList.getCustomersWithMatchingCardNumber(customers, startInterval, endInterval);
        customerPrinter.print(matchingCustomers);
        System.out.println('\n');


        // Task 4
        Book java = new Book(1, "java", new String[]{"Gosling"}, "Piter", 2010, 100, 10, "hard");
        Book sharp = new Book(2, "sharp", new String[]{"Gosling"},
                "Piter", 2009, 100, 10, "hard");
        Book idiot = new Book(5, "Idiot", new String[]{"Dostoevski"},
                "Piter", 1889, 122, 4, "soft");
        Book book1984 = new Book(5, "1984", new String[]{"Orwell"},
                "Piter", 1984, 112, 15, "soft");
        Book player = new Book(12, "Player", new String[]{"Dostoevski"},
                "Piter", 1887, 122, 10, "soft");

        BookRepository repository = new BookRepository(new Book[]{java, sharp, idiot, book1984, player});

        Book[] goslingBooks = repository.findByAuthor("Gosling");
        Book[] piterBooks = repository.findByPublisher("Piter");
        Book[] books2009 = repository.findByYear(2009);

        BookPrinter bookPrinter = new BookPrinter();

        bookPrinter.print(goslingBooks);
        System.out.println();
        bookPrinter.print(piterBooks);
        System.out.println();
        bookPrinter.print(books2009);
        System.out.println("\n");

        // Task 5
        Airline[] airlines = new Airline[]{
                new Airline("Paris", 1, "Ryanair", 15, 0, "Sunday"),
                new Airline("New-York", 14, "FlyEmirates", 2, 15, "Tuesday"),
                new Airline("Paris", 15, "Belavia", 12, 30, "Monday"),
                new Airline("Dubai", 17, "BritishAirlines", 18, 45, "Tuesday"),
        };

        AirlineList airlineList = new AirlineList();
        AirlinePrinter airlinePrinter = new AirlinePrinter();

        Airline[] flightsByDestination = airlineList.getFlightsArrayByDestination(airlines,"Paris");
        airlinePrinter.print(flightsByDestination);
        System.out.println();

        Airline[] flightsByDay = airlineList.getFlightsArrayByDay(airlines,"Tuesday");
        airlinePrinter.print(flightsByDay);

        System.out.println();

        Airline[] flightsByDayAndTime = airlineList.getFlightsArrayByDayAndTime(airlines,"Tuesday", 10,0);
        airlinePrinter.print(flightsByDayAndTime);



    }
}
