package Airline;

import Airline.Airline;

public class AirlineList {
    public AirlineList() {
    }

    public Airline[] getFlightsArrayByDestination(Airline[] airlines, String destination){
        Airline[] flightsByDestination = new Airline[airlines.length];
        int counter = 0;

        for(Airline airline: airlines){
            if(airline.getDestination().equals(destination)){
                flightsByDestination[counter] = airline;
                counter++;
            }
        }
        return flightsByDestination;
    }

    public Airline[] getFlightsArrayByDay(Airline[] airlines, String day){
        Airline[] flightsByDay = new Airline[airlines.length];
        int counter = 0;

        for(Airline airline: airlines){
            if(airline.getDay().equals(day)){
                flightsByDay[counter] = airline;
                counter++;
            }
        }
        return flightsByDay;
    }

    public Airline[] getFlightsArrayByDayAndTime(Airline[] airlines, String day, int hours, int minutes){
        Airline[] flightsByDayAndTime = new Airline[airlines.length];
        int counter = 0;

        for(Airline airline: airlines){
            if(airline.getDay().equals(day) && airline.getHours() > hours ||
                    ((airline.getDay().equals(day) && airline.getHours() == hours && airline.getMinutes() > minutes))){
                flightsByDayAndTime[counter] = airline;
                counter++;
            }
        }
        return flightsByDayAndTime;
    }
}
