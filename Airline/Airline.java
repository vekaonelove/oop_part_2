package Airline;

import java.util.Objects;

public class Airline {
    private String destination;
    private int flight;
    private String aircraft;
    private int hours;
    private int minutes;
    private String day;

    public Airline(String destination, int flight, String aircraft, int hours, int minutes, String day) {
        this.destination = destination;
        this.flight = flight;
        this.aircraft = aircraft;
        this.hours = hours;
        this.minutes = minutes;
        this.day = day;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getFlight() {
        return flight;
    }

    public void setFlight(int flight) {
        this.flight = flight;
    }

    public String getAircraft() {
        return aircraft;
    }

    public void setAircraft(String aircraft) {
        this.aircraft = aircraft;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Airline that = (Airline) o;

        if (flight != that.flight) return false;
        if (hours != that.hours) return false;
        if (minutes != that.minutes) return false;
        if (!Objects.equals(destination, that.destination)) return false;
        if (!Objects.equals(aircraft, that.aircraft)) return false;
        return Objects.equals(day, that.day);
    }

    @Override
    public int hashCode() {
        int result = destination != null ? destination.hashCode() : 0;
        result = 31 * result + flight;
        result = 31 * result + (aircraft != null ? aircraft.hashCode() : 0);
        result = 31 * result + hours;
        result = 31 * result + minutes;
        result = 31 * result + (day != null ? day.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Airline.AirlinePrinter{" +
                "destination='" + destination + '\'' +
                ", flight=" + flight +
                ", aircraft='" + aircraft + '\'' +
                ", hours=" + hours +
                ", minutes=" + minutes +
                ", day='" + day + '\'' +
                '}';
    }
}
