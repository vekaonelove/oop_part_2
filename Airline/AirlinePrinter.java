package Airline;

import Airline.Airline;

public class AirlinePrinter {
    public void print(Airline[] airlines){
        for(Airline airline: airlines){
            print(airline);
        }
    }

    public void print(Airline airline){
        if (airline != null){
            System.out.println(airline.toString());
        }
    }
}
