package Book;

public class BookRepository {
    private Book[] books;

    public BookRepository(Book[] books) {
        this.books = books;
    }

    public Book[] findByAuthor(String author){
        Book[] result = new Book[books.length];
        int count = 0;

        for(int i = 0; i < books.length; i++){
            Book book = books[i];
            if( book != null && contains(book.getAuthors(), author)){
                result[count++] = book;
            }
        }
        return extractSubarray(result,count);
    }

    private Boolean contains(String[] items, String target){
        for(String item: items){
            if(item.equals(target)){
                return true;
            }
        }
        return false;
    }

    public Book[] findByPublisher(String publisher){
        Book[] result = new Book[books.length];
        int count = 0;

        for(int i = 0; i < books.length; i++){
            Book book = books[i];
            if( book.getPublisher() != null && book != null && book.getPublisher().equals(publisher)){
                result[count++] = book;
            }
        }

        return extractSubarray(result,count);
    }

    public Book[] findByYear(int year){
        Book[] result = new Book[books.length];
        int count = 0;

        for(int i = 0; i < books.length; i++){
            Book book = books[i];
            if( book != null && book.getYear()==year){
                result[count++] = book;
            }
        }

        return extractSubarray(result,count);
    }

    private Book[] extractSubarray(Book[] source, int count){
        Book[] destination = new Book[count];
        System.arraycopy(source,0,destination,0,count);
        return destination;
    }

}
