package Book;

public class BookPrinter {
    public void print(Book[] books){
        for(Book book: books){
            print(book);
        }
    }

    public void print(Book book){
        System.out.println(book.toString());
    }
}
