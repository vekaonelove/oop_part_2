package Train;

public class TrainPrinter {
    public void print(Train[] trains){
        for(Train train: trains){
            print(train);
        }
    }

    public void print(Train train){
        System.out.println(train.toString());
    }

    public void printByNumber(int num, Train[] trains){
        for(Train train: trains){
            if(train.getNumber() == num){
                System.out.println(train);
            }
        }

    }
}