package Train;

public class TrainList {
    private Train[] trainList;

    public TrainList(Train[] trainList) {
        this.trainList = trainList;
    }


    public Train[] sortByTrainNumber(Train[] trainList) {

        int n = trainList.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (trainList[j].getNumber() > trainList[j + 1].getNumber()) {
                    Train temp = trainList[j];
                    trainList[j] = trainList[j + 1];
                    trainList[j + 1] = temp;
                }

            }
        }
        return trainList;
    }

    public Train[] sortByDestination(Train[] trainList) {
        int n = trainList.length;
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (trainList[j].getDestination().compareTo(trainList[j + 1].getDestination()) > 0 ||
                        (trainList[j].getDestination().compareTo(trainList[j + 1].getDestination()) == 0 && trainList[j].getHours() > trainList[j + 1].getHours()) ||
                        (trainList[j].getDestination().compareTo(trainList[j + 1].getDestination()) == 0 && trainList[j].getHours() == trainList[j + 1].getHours() &&
                                trainList[j].getMinutes() > trainList[j + 1].getMinutes())) {
                    Train temp = trainList[j];
                    trainList[j] = trainList[j + 1];
                    trainList[j + 1] = temp;
                }
            }
        }
        return trainList;
    }
}
