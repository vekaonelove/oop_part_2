package Train;

import java.util.Objects;

public class Train {
    private String destination;
    private int number;
    private int hours, minutes;

    public Train(String destination, int number, int hours, int minutes) {
        this.destination = destination;
        this.number = number;
        this.hours = hours;
        this.minutes = minutes;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        if(hours > 23 || hours < 1){
            this.hours = 0;
        }
        else{
            this.hours = hours;
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        if(minutes > 59 || minutes < 0){
            this.minutes = 0;
        }
        else{
            this.minutes = minutes;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Train train = (Train) o;

        if (number != train.number) return false;
        if (hours != train.hours) return false;
        if (minutes != train.minutes) return false;
        return Objects.equals(destination, train.destination);
    }

    @Override
    public int hashCode() {
        int result = destination != null ? destination.hashCode() : 0;
        result = 31 * result + number;
        result = 31 * result + hours;
        result = 31 * result + minutes;
        return result;
    }

    @Override
    public String toString() {
        return "Train.Train{" +
                "destination='" + destination + '\'' +
                ", number=" + number +
                ", hours=" + hours +
                ", minutes=" + minutes +
                '}';
    }


}
